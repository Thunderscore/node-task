# Node Task

This project contains node hello-world

## Installation steps
1. Copy the repository
2. Install dependencies
3. Start the application

## Terminal commands
```sh
git clone https://gitlab.com/Thunderscore/node-task
cd .\node-task
npm i
```

Start cmd: `npm run start`


## App should work this way

http://localhost:3000/
> Hello world

http://localhost:3000/add/
> Sum: 3

http://localhost:3000/subtract/
> Sum: 2