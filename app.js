const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a, b) => {
    return a + b;
}

// This function displays the sum from add()
app.get('/add/', (req, res) => {
    const x = add(1, 2)
    res.send(`Sum: ${x}`)
})

/**
 * This function subtracts two numbers
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const subtract = (a, b) => {
    return a - b;
}

// This function displays sum from subtract()
app.get('/subtract/', (req, res) => {
    const y = subtract(5, 3)
    res.send(`Sum: ${y}`)
})

// Welcome message
app.get('/', (req, res) => {
    res.send('Hello World!')
})

// Console log message
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})